<?php
namespace Application\Service;

// use GuzzleHttp\Client;
// use GuzzleHttp\Psr7\Request;
use Laminas\Http\Request;
use Laminas\Http\Client;

class ActorsService {

  function fetchAll(){
    $config = [
      'adapter'     => Client\Adapter\Curl::class,
      'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
    ];
    $uri = 'http://localhost:8765/api/sakila/actor';
    $client = new Client($uri, $config);
    return $client->send();    
  }
}

