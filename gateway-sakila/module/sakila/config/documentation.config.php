<?php
return [
    'sakila\\V1\\Rest\\Customers\\Controller' => [
        'description' => 'Lista de clientes da locadora',
    ],
    'sakila\\V1\\Rest\\Address\\Controller' => [
        'description' => 'Lista de endereços',
    ],
    'sakila\\V1\\Rest\\Country\\Controller' => [
        'description' => 'Lista de Estados',
    ],
    'sakila\\V1\\Rest\\City\\Controller' => [
        'description' => 'Lista de Cidades',
    ],
    'sakila\\V1\\Rest\\Language\\Controller' => [
        'description' => 'Lista de Idiomas',
    ],
    'sakila\\V1\\Rest\\Films\\Controller' => [
        'description' => 'Lista de Filmes',
    ],
    'sakila\\V1\\Rest\\Actors\\Controller' => [
        'description' => 'Lista de Atores',
        'collection' => [
            'GET' => [
                'description' => 'Recupera uma lista',
            ],
            'POST' => [
                'description' => 'Salvar um registro',
                'request' => '',
            ],
        ],
    ],
];
