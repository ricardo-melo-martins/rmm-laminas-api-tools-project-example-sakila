<?php
return [
    'service_manager' => [
        'factories' => [
            \sakila\V1\Rest\Actors\ActorsResource::class => \sakila\V1\Rest\Actors\ActorsResourceFactory::class,
            \sakila\V1\Rest\Films\FilmsResource::class => \sakila\V1\Rest\Films\FilmsResourceFactory::class,
            \sakila\V1\Rest\Language\LanguageResource::class => \sakila\V1\Rest\Language\LanguageResourceFactory::class,
            \sakila\V1\Rest\City\CityResource::class => \sakila\V1\Rest\City\CityResourceFactory::class,
            \sakila\V1\Rest\Country\CountryResource::class => \sakila\V1\Rest\Country\CountryResourceFactory::class,
            \sakila\V1\Rest\Address\AddressResource::class => \sakila\V1\Rest\Address\AddressResourceFactory::class,
            \sakila\V1\Rest\Customers\CustomersResource::class => \sakila\V1\Rest\Customers\CustomersResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'sakila.rest.actors' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/sakila/actors[/:actors_id]',
                    'defaults' => [
                        'controller' => 'sakila\\V1\\Rest\\Actors\\Controller',
                    ],
                ],
            ],
            'sakila.rest.films' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/sakila/films[/:films_id]',
                    'defaults' => [
                        'controller' => 'sakila\\V1\\Rest\\Films\\Controller',
                    ],
                ],
            ],
            'sakila.rest.language' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/sakila/language[/:language_id]',
                    'defaults' => [
                        'controller' => 'sakila\\V1\\Rest\\Language\\Controller',
                    ],
                ],
            ],
            'sakila.rest.city' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/sakila/city[/:city_id]',
                    'defaults' => [
                        'controller' => 'sakila\\V1\\Rest\\City\\Controller',
                    ],
                ],
            ],
            'sakila.rest.country' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/sakila/country[/:country_id]',
                    'defaults' => [
                        'controller' => 'sakila\\V1\\Rest\\Country\\Controller',
                    ],
                ],
            ],
            'sakila.rest.address' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/sakila/address[/:address_id]',
                    'defaults' => [
                        'controller' => 'sakila\\V1\\Rest\\Address\\Controller',
                    ],
                ],
            ],
            'sakila.rest.customers' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/sakila/customers[/:customers_id]',
                    'defaults' => [
                        'controller' => 'sakila\\V1\\Rest\\Customers\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'api-tools-versioning' => [
        'uri' => [
            0 => 'sakila.rest.actors',
            1 => 'sakila.rest.films',
            2 => 'sakila.rest.language',
            3 => 'sakila.rest.city',
            4 => 'sakila.rest.country',
            5 => 'sakila.rest.address',
            6 => 'sakila.rest.customers',
        ],
    ],
    'api-tools-rest' => [
        'sakila\\V1\\Rest\\Actors\\Controller' => [
            'listener' => \sakila\V1\Rest\Actors\ActorsResource::class,
            'route_name' => 'sakila.rest.actors',
            'route_identifier_name' => 'actors_id',
            'collection_name' => 'actors',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \sakila\V1\Rest\Actors\ActorsEntity::class,
            'collection_class' => \sakila\V1\Rest\Actors\ActorsCollection::class,
            'service_name' => 'actors',
        ],
        'sakila\\V1\\Rest\\Films\\Controller' => [
            'listener' => \sakila\V1\Rest\Films\FilmsResource::class,
            'route_name' => 'sakila.rest.films',
            'route_identifier_name' => 'films_id',
            'collection_name' => 'films',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \sakila\V1\Rest\Films\FilmsEntity::class,
            'collection_class' => \sakila\V1\Rest\Films\FilmsCollection::class,
            'service_name' => 'films',
        ],
        'sakila\\V1\\Rest\\Language\\Controller' => [
            'listener' => \sakila\V1\Rest\Language\LanguageResource::class,
            'route_name' => 'sakila.rest.language',
            'route_identifier_name' => 'language_id',
            'collection_name' => 'language',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \sakila\V1\Rest\Language\LanguageEntity::class,
            'collection_class' => \sakila\V1\Rest\Language\LanguageCollection::class,
            'service_name' => 'language',
        ],
        'sakila\\V1\\Rest\\City\\Controller' => [
            'listener' => \sakila\V1\Rest\City\CityResource::class,
            'route_name' => 'sakila.rest.city',
            'route_identifier_name' => 'city_id',
            'collection_name' => 'city',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \sakila\V1\Rest\City\CityEntity::class,
            'collection_class' => \sakila\V1\Rest\City\CityCollection::class,
            'service_name' => 'city',
        ],
        'sakila\\V1\\Rest\\Country\\Controller' => [
            'listener' => \sakila\V1\Rest\Country\CountryResource::class,
            'route_name' => 'sakila.rest.country',
            'route_identifier_name' => 'country_id',
            'collection_name' => 'country',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \sakila\V1\Rest\Country\CountryEntity::class,
            'collection_class' => \sakila\V1\Rest\Country\CountryCollection::class,
            'service_name' => 'country',
        ],
        'sakila\\V1\\Rest\\Address\\Controller' => [
            'listener' => \sakila\V1\Rest\Address\AddressResource::class,
            'route_name' => 'sakila.rest.address',
            'route_identifier_name' => 'address_id',
            'collection_name' => 'address',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \sakila\V1\Rest\Address\AddressEntity::class,
            'collection_class' => \sakila\V1\Rest\Address\AddressCollection::class,
            'service_name' => 'address',
        ],
        'sakila\\V1\\Rest\\Customers\\Controller' => [
            'listener' => \sakila\V1\Rest\Customers\CustomersResource::class,
            'route_name' => 'sakila.rest.customers',
            'route_identifier_name' => 'customers_id',
            'collection_name' => 'customers',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \sakila\V1\Rest\Customers\CustomersEntity::class,
            'collection_class' => \sakila\V1\Rest\Customers\CustomersCollection::class,
            'service_name' => 'customers',
        ],
    ],
    'api-tools-content-negotiation' => [
        'controllers' => [
            'sakila\\V1\\Rest\\Actors\\Controller' => 'Json',
            'sakila\\V1\\Rest\\Films\\Controller' => 'HalJson',
            'sakila\\V1\\Rest\\Language\\Controller' => 'HalJson',
            'sakila\\V1\\Rest\\City\\Controller' => 'HalJson',
            'sakila\\V1\\Rest\\Country\\Controller' => 'HalJson',
            'sakila\\V1\\Rest\\Address\\Controller' => 'HalJson',
            'sakila\\V1\\Rest\\Customers\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'sakila\\V1\\Rest\\Actors\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Films\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Language\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'sakila\\V1\\Rest\\City\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Country\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Address\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Customers\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'sakila\\V1\\Rest\\Actors\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Films\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Language\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/json',
            ],
            'sakila\\V1\\Rest\\City\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Country\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Address\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/json',
            ],
            'sakila\\V1\\Rest\\Customers\\Controller' => [
                0 => 'application/vnd.sakila.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'api-tools-hal' => [
        'metadata_map' => [
            \sakila\V1\Rest\Actors\ActorsEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.actors',
                'route_identifier_name' => 'actors_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializable::class,
            ],
            \sakila\V1\Rest\Actors\ActorsCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.actors',
                'route_identifier_name' => 'actors_id',
                'is_collection' => true,
            ],
            \sakila\V1\Rest\Films\FilmsEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.films',
                'route_identifier_name' => 'films_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializable::class,
            ],
            \sakila\V1\Rest\Films\FilmsCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.films',
                'route_identifier_name' => 'films_id',
                'is_collection' => true,
            ],
            \sakila\V1\Rest\Language\LanguageEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.language',
                'route_identifier_name' => 'language_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializable::class,
            ],
            \sakila\V1\Rest\Language\LanguageCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.language',
                'route_identifier_name' => 'language_id',
                'is_collection' => true,
            ],
            \sakila\V1\Rest\City\CityEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.city',
                'route_identifier_name' => 'city_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializable::class,
            ],
            \sakila\V1\Rest\City\CityCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.city',
                'route_identifier_name' => 'city_id',
                'is_collection' => true,
            ],
            \sakila\V1\Rest\Country\CountryEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.country',
                'route_identifier_name' => 'country_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializable::class,
            ],
            \sakila\V1\Rest\Country\CountryCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.country',
                'route_identifier_name' => 'country_id',
                'is_collection' => true,
            ],
            \sakila\V1\Rest\Address\AddressEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.address',
                'route_identifier_name' => 'address_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializable::class,
            ],
            \sakila\V1\Rest\Address\AddressCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.address',
                'route_identifier_name' => 'address_id',
                'is_collection' => true,
            ],
            \sakila\V1\Rest\Customers\CustomersEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.customers',
                'route_identifier_name' => 'customers_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializable::class,
            ],
            \sakila\V1\Rest\Customers\CustomersCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'sakila.rest.customers',
                'route_identifier_name' => 'customers_id',
                'is_collection' => true,
            ],
        ],
    ],
];
