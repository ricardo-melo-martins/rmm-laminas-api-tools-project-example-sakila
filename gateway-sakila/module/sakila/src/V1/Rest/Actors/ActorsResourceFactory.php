<?php
namespace sakila\V1\Rest\Actors;

use Interop\Container\ContainerInterface;
use Application\Service\ActorsService;
class ActorsResourceFactory
{
    public function __invoke($services)
    {
        $service = $services->get(ActorsService::class);
        return new ActorsResource($service);
    }
}
