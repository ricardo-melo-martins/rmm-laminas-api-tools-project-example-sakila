<?php
namespace sakila\V1\Rest\Films;

class FilmsResourceFactory
{
    public function __invoke($services)
    {
        return new FilmsResource();
    }
}
