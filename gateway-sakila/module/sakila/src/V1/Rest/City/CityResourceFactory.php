<?php
namespace sakila\V1\Rest\City;

class CityResourceFactory
{
    public function __invoke($services)
    {
        return new CityResource();
    }
}
