<?php
namespace sakila\V1\Rest\Language;

class LanguageResourceFactory
{
    public function __invoke($services)
    {
        return new LanguageResource();
    }
}
