# RMM Skeleton Laminas Api Rest Service Layer

# Api Sakila

Visão Documentação
![Imagem](/docs/images/swagger-sakila.png 'Gateway Sakila')

# Testar

Clone este repositório e no shell digite

`
cd gateway-sakila/public
$ php -S 0.0.0.0:8080 -t index.php
`